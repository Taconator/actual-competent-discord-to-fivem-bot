import asyncio
import base64
import json

import aiohttp
import requests


class Info:
    @staticmethod
    def Get(IP : str, Port : int):
        try:
            return ServerInfo(requests.get(f'http://{IP}:{Port}/info.json').json())
        except Exception as E:
            print(E)
            return False

    @staticmethod
    async def GetAsync(IP : str, Port : int):
        async with aiohttp.ClientSession() as Session:
            try:
                Info = await Session.get(f'http://{IP}:{str(Port)}/info.json')
                Data = await Info.read()
                Info = json.loads(Data)
                Session.close()
                return ServerInfo(Info)
            except Exception as E:
                await asyncio.sleep(0.05)
                print(E)
                Session.close()
                return False


class ServerInfo(dict):
    def __init__(self, Data : dict):
        dict.__init__(self)
        self.update(Data)

    def DecodeIcon(self):
        with open('CurrentIcon.png', 'wb') as Image:
            Image.write(base64.b64decode(self['icon']))
        return 'CurrentIcon.png'


class PlayerData(list):
    def __init__(self, Data : list):
        list.__init__(self)
        self.__iadd__(Data)

    def ToMarkdown(self, MessageIfEmpty : str = '', NoID = True):
        Out = f'```markdown\nPlayers\n{"=" * 7}\n\n'
        i = 0
        if NoID:
            while i < len(self):
                Out += f'{i + 1}.\t[{self[i]["id"]}][{self[i]["name"]}] - {self[i]["ping"]} ping\n'
                i += 1
        else:
            while i < len(self):
                Out += f'{i + 1}.\t[{self[i]["id"]}][{self[i]["name"]}][{self[i]["identifiers"]}] - {self[i]["ping"]} ping\n'
                i += 1
        return Out + (f'{MessageIfEmpty}```' if len(self) == 0 else '```')


class Players:
    @staticmethod
    def Get(IP : str, Port : int):
        try:
            return PlayerData(requests.get(f'http://{IP}:{Port}/players.json').json())
        except Exception as E:
            print(E)
            return False

    @staticmethod
    async def GetAsync(IP : str, Port : int):
        async with aiohttp.ClientSession() as Session:
            try:
                Players = await Session.get(f'http://{IP}:{Port}/players.json')
                Data = await Players.read()
                Players = json.loads(Data)
                return PlayerData(Players)
            except Exception as E:
                await asyncio.sleep(0.05)
                print('Error')
                print(E)
                return False