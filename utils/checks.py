import discord
from discord.ext import commands

class NotOwner(commands.CommandError) : pass
class WrongGuild(commands.CommandError) : pass

MyAccounts = [
    157619512710135808, # Taconator (Main)
    370729752350294016, # Suzuki Desune (Alt)
    326065793936785410 # Kezookoo (Alt)
]

def IsOwnerCheck(Message : discord.Message):
    if Message.author.id in MyAccounts:
        return True
    raise NotOwner()

def GuildCheck(Message : discord.Message):
    if Message.guild.id == 361553346580185089:
        return True
    raise WrongGuild()

def IsOwner():
    return commands.check(lambda ctx: IsOwnerCheck(ctx.message))

def RightGuild():
    return commands.check(lambda ctx: GuildCheck(ctx.message))