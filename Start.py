import asyncio
import ctypes
import datetime
import json
from pathlib import Path

import discord
from discord.ext import commands
from utils import checks

async def Start():
    Bot = DiscordToFiveMBot()
    Bot.Title = 'Discord To FiveM | Not Yet Started'
    with open('config.json') as File:
        Config = json.load(File)
    if Config['token'] == '' or Config['dtfpw'] == '':
        print('Failed to start bot, either the token or DiscordToFiveM password is missing')
        return
    Bot.DTFPW = Config['dtfpw']
    try:
        await Bot.start(Config['token'])

    except Exception:
        await Bot.logout()


class DiscordToFiveMBot(commands.Bot):
    class InsufficientPermissions(commands.CommandError) : pass
    class SendFailed(commands.CommandError) : pass
    class NoID(commands.CommandError) : pass

    def __init__(self):
        super().__init__(command_prefix = self.GetPrefix, description = 'Actual Competent Discord to FiveM Bot', case_insensitive = True)
        self._Title = ''
        self.StartTime = None
        self.ApplicationInfo = None
        Title = 'Discord To FiveM Bot | Initialized'

        self.loop.create_task(self.OnConnect())
        self.loop.create_task(self.LoadExtensions())

    @classmethod
    def GetPrefix(self, Bot, Message):
        Prefixes = ['!!']

        if not Message.guild:
            return '!!'
        return commands.when_mentioned_or(*Prefixes)(Bot, Message)

    async def OnConnect(self):
        await self.wait_until_ready()
        self._Title = 'Discord To FiveM Bot | Connected to Discord'
        self.StartTime = datetime.datetime.utcnow()

    @property
    def Title(self):
        return self._Title

    @Title.setter
    def Title(self, NewValue):
        self._Title = NewValue
        ctypes.windll.kernel32.SetConsoleTitleW(self._Title)
        

    async def on_ready(self):
        print('-' * 10)
        self.ApplicationInfo = await self.application_info()
        self.Prefix = self.GetPrefix(self, await self.get_channel(362421228780257280).get_message(471447056859398158))[2]
        print(f'Logged in as: {self.user.name}\nUsing discord.py version: {discord.__version__}\nOwner: {self.ApplicationInfo.owner}')
        await self.change_presence(status=discord.Status.online, activity=discord.Game(f'{self.Prefix}cmdlist'))
        self.remove_command('help')

    async def LoadExtensions(self):
        await self.wait_until_ready()
        await asyncio.sleep(1)
        Cogs = [x.stem for x in Path('cogs').glob('*.py')]
        print('-' * 10)
        for Extension in Cogs:
            try:
                self.Title = f'Discord To FiveM Bot | Loading {Extension} extension'
                self.load_extension(f'cogs.{Extension}')
                print(f'Loaded {Extension} extension')
            except Exception as E:
                Error = f'{Extension}\n {type(E).__name__} : {E}'
                print(f'Failed to load extension {Error}')
            print('-' * 10 + '\n')
        self.Title = 'Discord To FiveM Bot'

    async def on_command_error(self, ctx, Error):
        if isinstance(Error, commands.CommandOnCooldown):
            Wait = Error.retry_after
            await ctx.send(f':x: **Command is on cooldown, retry in {round(Wait, 2)} seconds')
        elif isinstance(Error, self.InsufficientPermissions) or isinstance(Error, checks.NotOwner):
            await ctx.send(f':x: **You are not allowed to execute that commmand**')
        elif isinstance(Error, checks.WrongGuild):
            await ctx.send(':x: **You\'re executing this command in the wrong guild**\nOnly allowed to be executed in The Tofu Shop')
        elif isinstance(Error, self.SendFailed):
            await ctx.send(':x: **Failed to send message to server**')
        else:
            Embed = discord.Embed(title='Exception Has Occurred', description='Please report to Taconator#5417\nInclude the exact command you ran', color=0xFF0000)
            Embed.add_field(name='Exception Type', value=Error.__class__)
            Embed.add_field(name='Exception Value', value=Error.__str__())
            try:
                Embed.add_field(name='Traceback Details', value=f'Line Exception Occurred On\t{Error.__traceback__.tb_lineno}\nIndex of Last Attempted Instruction\t{Error.__traceback__.tb_lasti}\nFrame Line Number\t{Error.__traceback__.tb_frame.f_lineno}')
            except Exception:
                pass
            await ctx.send(embed=Embed)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(Start())