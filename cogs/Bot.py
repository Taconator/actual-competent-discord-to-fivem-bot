import json
import random
import re

import aiohttp
import discord
from discord.ext import commands
from utils import checks
from utils.fxserver import Players

class InsufficientPermissions(commands.CommandError) : pass
class SendFailed(commands.CommandError) : pass
class NoID(commands.CommandError) : pass

class BotCommands:
    def __init__(self, Bot):
        self.Bot = Bot
        self.Secure_Random = random.SystemRandom()

    def PermsCheck(self, Member : discord.Member):
        AdministratorRole = self.Bot.get_guild(361553346580185089).get_role(361554304315686922)
        ModeratorRole = self.Bot.get_guild(361553346580185089).get_role(364823855044231168)
        if AdministratorRole in Member.roles or ModeratorRole in Member.roles:
            return True
        else:
            raise self.Bot.InsufficientPermissions()

    async def HTTPRequest(self, Request, AdditionalPath = None, Sender = 'TofuShopSystem'):
        if AdditionalPath is not None and AdditionalPath != '':
            AdditionalPath = '/' + AdditionalPath
        else:
            AdditionalPath = ''

        async with aiohttp.ClientSession() as Session:
            return await Session.get(f'http://149.56.184.98:30120/DiscordToFiveM/{self.Bot.DTFPW}/{Sender}/{Request}{AdditionalPath}')

    @commands.command(name='cmdlist', case_insensitive=True, aliases=['help', 'cmds'])
    @checks.RightGuild()
    async def CMDList(self, ctx):
            Embed = discord.Embed(color = int("%06x" % self.Secure_Random.randint(0, 0xFFFFFF), 16), title = self.Bot.user.name + ' Commands List')
            Embed.set_footer(text = 'Created by Taconator#5417', icon_url = self.Bot.get_user(157619512710135808).avatar_url)
            Embed.add_field(name = f'{self.Bot.Prefix}cmdlist, {self.Bot.Prefix}help, {self.Bot.Prefix}cmds', value = 'Shows what you currently see')
            Embed.add_field(name = f'{self.Bot.Prefix}chkcon', value = 'Checks to see if the bot can connect to the server')
            Embed.add_field(name = f'{self.Bot.Prefix}getclients, {self.Bot.Prefix}clients', value = 'Gets all clients connected to the server')
            Embed.add_field(name = f'{self.Bot.Prefix}send [Message]', value = 'Sends whatever you replace `[Message]` with to the server')
            Embed.add_field(name = f'{self.Bot.Prefix}kick [ID] [Reason]', value = 'Kicks a client using their in-game ID and also allows you to specify a reason')
            Embed.add_field(name = f'{self.Bot.Prefix}ban [ID] [Reason]', value = 'Bans a client using their in-game ID and also allows you to specify a reason')
            await ctx.send(embed = Embed)

    @commands.command(name='chkcon', case_insensitive=True)
    @checks.RightGuild()
    async def CheckConnection(self, ctx):
        async with ctx.channel.typing():
            Response = await self.HTTPRequest('chkcon')
            ResponseText = await Response.text()
            Response.close()
            await ctx.send(':white_check_mark: **Connection to server was successful**' if ResponseText == '"Connection successful"' else ':x: **Failed to connect to server**')

    @commands.command(name='getclients', case_insensitive=True, aliases=['clients', 'players'])
    @checks.RightGuild()
    async def GetClients(self, ctx):
        async with ctx.channel.typing():
            PlayersList = await Players.GetAsync('149.56.184.98', 30120)
            await ctx.send(PlayersList.ToMarkdown('Server is empty'))

    @commands.command(name='send')
    @checks.RightGuild()
    async def SendMessage(self, ctx):
        if self.PermsCheck(ctx.author):
            Message = ctx.message.content.split(' ', 1)[1]
            if len(Message) > 0 and Message != '<MESSAGE>':
                Request = await self.HTTPRequest('send', Message)
                Request.close()
                await ctx.send(':white_check_mark: **Message sent successfully**')
            else:
                raise self.Bot.SendFailed()

    @commands.command(name='sendon')
    @checks.RightGuild()
    async def SendMessageOwnName(self, ctx):
        if self.PermsCheck(ctx.author):
            Message = ctx.message.content.split(' ', 1)[1]
            if len(Message) > 0 and Message != '<MESSAGE>':
                async with ctx.message.typing():
                    Request = await self.HTTPRequest('send', Message, ctx.author.display_name)
                    Request.close()
                    await ctx.send(':white_check_mark: **Message sent successfully**')
            else:
                raise self.Bot.SendFailed()

    @commands.command(name='sendrgb')
    @checks.RightGuild()
    async def SendMessageRGB(self, ctx, R : int, G : int, B : int, Message : str):
        if self.PermsCheck(ctx.author):
            Message = ctx.message.content.split(' ', 4)[4]
            if len(Message) > 0 and Message != '<MESSAGE>':
                async with ctx.channel.typing():
                    Response = await self.HTTPRequest('sendrgb', f'{str(R)}/{str(G)}/{str(B)}/{Message}')
                    Data = await Response.read()
                    Response.close()
                    Response = json.loads(Data)
                    await ctx.send(f'{":white_check_mark:" if "success" in Response["Message"] else ":x:"} **{Response["Message"]}**\n\n**Details**\n{Response["Details"]}\n\n**Additional Information**\n{Response["AdditionalInfo"]}')
                    await ctx.message.delete()

    @commands.command(name='sendonrgb')
    @checks.RightGuild()
    async def SendMessageRGBOwnName(self, ctx, R : int, G : int, B : int, Message : str):
        if self.PermsCheck(ctx.author):
            Message = ctx.message.content.split(' ', 4)[4]
            if len(Message) > 0 and Message != '<MESSAGE>':
                async with ctx.channel.typing():
                    Response = await self.HTTPRequest('sendrgb', f'{str(R)}/{str(G)}/{str(B)}/{Message}', ctx.author.display_name)
                    Data = await Response.read()
                    Response.close()
                    Response = json.loads(Data)
                    await ctx.send(f'{":white_check_mark:" if "success" in Response["Message"] else ":x:"} **{Response["Message"]}**\n\n**Details**\n{Response["Details"]}\n\n**Additional Information**\n{Response["AdditionalInfo"]}')
                    await ctx.message.delete()

    @commands.command(name='ban')
    @checks.RightGuild()
    async def Ban(self, ctx):
        if self.PermsCheck(ctx.author):
            Parts = ctx.message.content.split(' ', 2)
            if len(Parts) < 2 or Parts[1] == '' or Parts[1] == None:
                raise self.Bot.NoID()
            ID = Parts[1]
            Reason = ''
            if len(Parts) == 3:
                Reason = Parts[2]
            async with ctx.channel.typing():
                Response = await self.HTTPRequest('ban', f'{ID}/{Reason}')
                ResponseText = await Response.text()
                Response.close()
                if ResponseText == 'null': # No one with ID found
                    await ctx.send(f':x: **Failed to ban player, it seems there\'s no one with ID {ID}**')
                    return
                BannedPlayer = re.sub(r'("Banned |")', '', ResponseText) # Replaces "Banned  and " with nothing to get the banned player
                await ctx.send(f':white_check_mark: **Successfully banned __{BannedPlayer}__ ({ID})**\n**Reason:** {Reason}')

    @commands.command(name='kick')
    @checks.RightGuild()
    async def Kick(self, ctx):
        if self.PermsCheck(ctx.author):
            Parts = ctx.message.content.split(' ', 2)
            if len(Parts) < 2 or Parts[1] == '' or Parts[1] == None:
                raise self.Bot.NoID()
            ID = Parts[1]
            Reason = ''
            if len(Parts) == 3:
                Reason = Parts[2]
            async with ctx.channel.typing():
                Response = await self.HTTPRequest('kick', f'{ID}/{Reason}')
                ResponseText = await Response.text()
                Response.close()
                if ResponseText == 'null': # No one with ID found
                    await ctx.send(f':x: **Failed to kick player, it seems there\'s no one with ID {ID}**')
                    return
                KickedPlayer = re.sub(r'("Kicked |")', '', ResponseText) # Replaces "Kicked and " with nothing to get the kicked player
                await ctx.send(f':white_check_mark: **Successfully kicked __{KickedPlayer}__ ({ID})**\n**Reason:** {Reason}')

    async def on_message(self, Message):
        if Message.guild.id == 361553346580185089 and Message.channel.id == 434593216038174750 and not Message.author.bot:
            Request = await self.HTTPRequest('send', Message.content, Message.author.display_name)
            Request.close()
            await Message.delete()

    @commands.command(name='weather', aliases=['setweather'])
    @checks.RightGuild()
    async def Weather(self, ctx, Weather : str = ''):
        if self.PermsCheck(ctx.author):
            Response = await self.HTTPRequest('weather', Weather)
            Data = await Response.read()
            Response.close()
            Response = json.loads(Data)
            await ctx.send(f'{":white_check_mark:" if "Success" in Response["Message"] else ":x:"} **{Response["Message"]}**\n**{Response["Details"]}**\n**Additional Info:** {chr(10).join(Response["AdditionalInfo"].split(";"))}')

    @commands.command(name='time', aliases=['settime'])
    @checks.RightGuild()
    async def Time(self, ctx, Hours : int, Minutes : int = -1, Seconds : int = -1):
        if self.PermsCheck(ctx.author):
            AdditionalPath = str(Hours)
            if Minutes == -1:
                pass
            elif Minutes != -1 and Seconds != -1:
                AdditionalPath += f'/{Minutes}/{Seconds}'
            else:
                AdditionalPath += f'/{Minutes}'
            Response = await self.HTTPRequest('time', AdditionalPath)
            Data = await Response.read()
            Response.close()
            Response = json.loads(Data)
            await ctx.send(f'{":white_check_mark:" if "Success" in Response["Message"] else ":x:"} **{Response["Message"]}**\n**{Response["Details"]}**\n**Additional Info:** {chr(10).join(Response["AdditionalInfo"].split(";"))}')



def setup(Bot):
    Bot.add_cog(BotCommands(Bot))