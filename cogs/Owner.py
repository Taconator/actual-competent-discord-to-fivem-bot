import os
import io
import sys
import contextlib

from discord.ext import commands
from utils import checks

@contextlib.contextmanager
def STDOutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = io.StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

class Owner:
    def __init__(self, Bot):
        self.Bot = Bot

    UsingPython = False

    @commands.command(name='is', case_insensitive=True)
    @checks.IsOwner()
    async def InstantShutdown(self, ctx):
        await ctx.message.delete()
        await self.Bot.logout()

    @commands.command(name='restart', case_insensitive=True)
    @checks.IsOwner()
    async def Restart(self, ctx):
        await ctx.message.delete()
        os.system('start cmd /c title Discord To FiveM Bot & call C:/users/bunta/desktop/StartRewriteEnv.bat & cls & Start.py & exit')
        await self.Bot.logout()
    
    @commands.command(name='getrole', case_insensitive=True)
    @checks.IsOwner()
    async def GetRole(self, ctx, Role : int):
        Role = ctx.guild.get_role(str(Role))
        print(Role)

    @commands.command(name='py', case_insensitive=True)
    @checks.IsOwner()
    async def Py(self, ctx):
        self.UsingPython = True
        await ctx.send('**Now interpreting everything sent by you as Python to be executed**')
    
    async def on_message(self, Message):
        if not Message.author.id == 157619512710135808:
            return
        if self.UsingPython:
            if Message.content.lower() == 'exit()':
                self.UsingPython = False
                await Message.channel.send('**Exiting Python interpreter**')
                return
            try:
                with STDOutIO() as S:
                    exec(Message.content)
                await Message.channel.send(S.getvalue())
            except Exception as E:
                await Message.channel.send(':x: **Exception Occurred**\n\n' + str(E))
            
        

def setup(Bot):
    Bot.add_cog(Owner(Bot))